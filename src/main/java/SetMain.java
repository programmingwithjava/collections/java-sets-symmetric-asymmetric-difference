import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class SetMain {
    public static void main(String[] args) {
        Set<Integer> squares = new HashSet<Integer>();
        Set<Integer> qubes = new HashSet<Integer>();

        for(int i=1; i<=100; i++) {
            squares.add(i*i);
            qubes.add(i*i*i);
        }

        /*
        * bulk operations in sets are destructive. they modify the sets they are called upon.
        * so we create a new set and do the operations on it.
        * */
        Set<Integer> union = new HashSet<Integer>(squares);
        union.addAll(qubes);

        System.out.println("union contains " + union.size() + " elements.");

        /*
        * in set theory there are 2 differences. symmetric and asymmetric.
        * Set interface defines a way to obtain the asymmetric difference.
        *
        * here it is called asymmetric difference because setA-setB is not equal to setB-setA.
        * */
        Set<Integer> intersection = new HashSet<Integer>(squares);
        intersection.retainAll(qubes);

        System.out.println("intersection containes " + intersection.size() + " elements.");

        for(int i : intersection){
            System.out.println(i + " is square of " + Math.sqrt(i) + " and cube of " + Math.cbrt(i));
        }
        /*
        * Arrays class provides an asList method that is used to return a list view of all the elements
        * in the array. it uses an ArrayList to do so.
        * */
        Set<String> words = new HashSet<String>();
        String sentence = "take these words and put it in a set";
        String[] arrayWrods = sentence.split(" ");
        words.addAll(Arrays.asList(arrayWrods));

        /*
        * remember output will not be in any particular order
        * */
        for(String s: words){
            System.out.println(s);
        }

        /*
        * here we create 2 sets and display the asymmetric differences between the two.
        * */
        Set<String> cars = new HashSet<String>();
        Set<String> bikes = new HashSet<String>();

        String[] carBrands = {"mercedes", "bmw", "audi", "toyota", "citroen", "renault", "honda", "suzuki"};
        String[] bikeBrands = {"yamaha", "honda", "suzuki","kawasaki","KTM", "triumph", "bmw"};

        cars.addAll(Arrays.asList(carBrands));
        bikes.addAll(Arrays.asList(bikeBrands));

        System.out.println("cars - bikes");
        Set<String> diff1 = new HashSet<String>(cars);
        diff1.removeAll(bikes);
        printSet(diff1);

        System.out.println("bikes - cars");
        Set<String> diff2 = new HashSet<String>(bikes);
        diff2.removeAll(cars);
        printSet(diff2);

        /*
        * symmetric difference can be defined as the union of the 2 sets minus the intersection.
        * let's calculate the symmetric difference.
        * */
        Set<String> unionCB = new HashSet<String>(cars);
        unionCB.addAll(bikes);

        Set<String> intersectionCB = new HashSet<String>(cars);
        intersectionCB.retainAll(bikes);

        System.out.println("symmetric difference");
        unionCB.removeAll(intersectionCB);
        printSet(unionCB);

        /*
        * containsAll is used to determine if one set is a subset of another. this bulk method is not destructive as the
        * rest of the bulk methods. this method does not modify the sets.
        * */
        if(cars.containsAll(bikes)){
            System.out.println("bikes is a subset of cars.");
        }

        if(bikes.containsAll(cars)){
            System.out.println("cars is a subset of bikes.");
        }

        if(cars.containsAll(intersectionCB)){
            System.out.println("intersectionCB is a subset of cars.");
        }

        if(bikes.containsAll(intersectionCB)){
            System.out.println("intersectionCB is a subset of bikes.");
        }

    }

    private static void printSet(Set<String> s){
        System.out.println("\t");
        for(String w: s){
            System.out.print(w + " ");
        }
        System.out.println("");
    }
}
